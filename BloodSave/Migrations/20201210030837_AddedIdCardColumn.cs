﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BloodSave.Migrations
{
    public partial class AddedIdCardColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdCard",
                table: "Donors",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IdCard",
                table: "Donors");
        }
    }
}
