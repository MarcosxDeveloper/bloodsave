﻿using BloodSave.Models;
using Microsoft.AspNetCore.Identity;
using System;

namespace BloodSave.Data
{
    public class ApplicationDbInitializer
    {
        public static void SeedData(
            ApplicationDbContext context,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            string[] roles = new string[] {
                "Donor",
                "BloodBank",
                "Administrator"
            };
            SeedRoles(roleManager, roles);

            string[] donors = new string[] { 
                "donor01@bloodsafe.com",
                "donor02@bloodsafe.com",
                "donor03@bloodsafe.com",
                "donor04@bloodsafe.com",
                "donor05@bloodsafe.com" 
            };
            SeedUsersWithRole(context, userManager, donors, "App123..", "Donor");

            string[] bloodbanks = new string[] {
                "bloodbank01@bloodsafe.com",
                "bloodbank02@bloodsafe.com",
                "bloodbank03@bloodsafe.com"
            };
            SeedUsersWithRole(context, userManager, bloodbanks, "App123..", "BloodBank");

            string[] administrators = new string[] {
                "admin01@bloodsafe.com",
            };
            SeedUsersWithRole(context, userManager, administrators, "App123..", "Administrator");
        }

        private static void SeedRoles(RoleManager<IdentityRole> roleManager, string[] names)
        {
            foreach (var name in names)
            {
                if (roleManager.FindByNameAsync(name).Result == null)
                {
                    IdentityRole role = new IdentityRole
                    {
                        Name = name,
                        NormalizedName = name.ToUpper()
                    };
                    roleManager.CreateAsync(role).Wait();
                }
            }
        }

        private static void SeedUsersWithRole(
            ApplicationDbContext context,
            UserManager<IdentityUser> userManager,
            string[] emails,
            string defaultPassword,
            string defaultRole)
        {
            Random r = new Random();

            foreach (var email in emails)
            {
                if (userManager.FindByEmailAsync(email).Result == null)
                {
                    IdentityUser user = new IdentityUser
                    {
                        UserName = email,
                        NormalizedUserName = email.ToUpper(),
                        Email = email,
                        NormalizedEmail = email.ToUpper(),
                    };

                    IdentityResult result = userManager.CreateAsync(user, defaultPassword).Result;

                    if (result.Succeeded)
                    {
                        userManager.AddToRoleAsync(user, defaultRole).Wait();
                    }

                    if(defaultRole == "Donor")
                    {
                        context.Donors.Add(new Donor
                        {
                            IdCard = "000-0000000-0",
                            FirstName = "DONOR",
                            LastName = r.Next(10).ToString(),
                            BloodType = (BloodTypes)r.Next(1, 8),
                            UserId = user.Id
                        });
                    }
                    else if(defaultRole == "BloodBank")
                    {
                        context.BloodBanks.Add(new BloodBank
                        {
                            Name = "BLOODBANK " + r.Next(10),
                            Location = "TEST LOCATION",
                            UserId = user.Id
                        });
                    }
                }
            }
        }
    }
}

