﻿using System.ComponentModel.DataAnnotations;

namespace BloodSave.Models
{
    public class DonationViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }

        [Display(Name = "Banco de Sangre")]
        public string BloodBank { get; set; }

        [Display(Name = "Ubicacion")]
        public string Location { get; set; }

        [Display(Name = "Cantidad de Sangre (ml)")]
        public int BloodQuantity { get; set; }
    }
}
