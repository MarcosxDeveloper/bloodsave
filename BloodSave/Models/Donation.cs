﻿using System.ComponentModel.DataAnnotations;

namespace BloodSave.Models
{
    public class Donation
    {
        public int Id { get; set; }

        public int DonorId { get; set; }

        [Display(Name = "Donante")]
        public Donor Donor { get; set; }

        public int BloodBankId { get; set; }

        [Display(Name = "Banco de Sangre")]
        public BloodBank BloodBank { get; set; }

        [Display(Name = "Cantidad de Sangre (ml)")]
        public int BloodQuantity { get; set; }
    }
}
