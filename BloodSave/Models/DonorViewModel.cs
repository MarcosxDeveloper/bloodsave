﻿using System.ComponentModel.DataAnnotations;

namespace BloodSave.Models
{
    public class DonorViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Nombre Completo")]
        public string FullName { get; set; }

        [Display(Name = "Tipo de Sangre")]
        public BloodTypes BloodType { get; set; }

        [Display(Name = "Cantidad de donaciones")]
        public int DonationQuantity { get; set; }
    }
}
