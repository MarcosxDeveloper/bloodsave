﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BloodSave.Models
{
    public class Donor
    {
        public int Id { get; set; }

        [Display(Name = "Cedula")]
        public string IdCard { get; set; }
        
        [Display(Name = "Primer Nombre")]
        public string FirstName { get; set; }
        
        [Display(Name = "Segundo Nombre")]
        public string LastName { get; set; }
        
        [Display(Name = "Tipo de Sangre")]
        public BloodTypes BloodType { get; set; }
        
        public string UserId { get; set; }
        
        [Display(Name = "Donaciones")]
        public ICollection<Donation> Donations { get; set; }
    }

    public enum BloodTypes
    {
        ONegative,
        OPositive,
        ANegative,
        APositive,
        BNegative,
        BPositive,
        ABNegative,
        ABPositive
    }
}
