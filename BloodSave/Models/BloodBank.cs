﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BloodSave.Models
{
    public class BloodBank
    {
        public int Id { get; set; }

        [Display(Name = "Nombre")]
        public string Name { get; set; }

        [Display(Name = "Ubicacion")]
        public string Location { get; set; }

        public string UserId { get; set; }

        [Display(Name = "Donaciones")]
        public ICollection<Donation> Donations { get; set; }
    }
}
