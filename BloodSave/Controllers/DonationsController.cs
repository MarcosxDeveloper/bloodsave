﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BloodSave.Data;
using BloodSave.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;

namespace BloodSave.Controllers
{
    [Authorize]
    public class DonationsController : Controller
    {
        private readonly ILogger<DonationsController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public DonationsController(
            ILogger<DonationsController> logger,
            ApplicationDbContext context,
            UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        // GET: Donations
        public async Task<IActionResult> Index()
        {
            List<DonationViewModel> donations = await _context.Donations
                .Include(m => m.Donor)
                .Include(m => m.BloodBank)
                .Select(m => new DonationViewModel
                {
                    Id = m.Id,
                    FullName = $"{m.Donor.FirstName} {m.Donor.LastName}",
                    BloodBank = m.BloodBank.Name,
                    Location = m.BloodBank.Location,
                    BloodQuantity = m.BloodQuantity
                })
                .ToListAsync();

            return View(donations);
        }

        // GET: Donations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donation = await _context.Donations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donation == null)
            {
                return NotFound();
            }

            return View(donation);
        }

        // GET: Donations/Create
        public async Task<IActionResult> Create(int id)
        {
            var userId = _userManager.GetUserId(User);
            var donor = await _context.Donors.FirstOrDefaultAsync(m => m.Id == id);
            var bloodbank = await _context.BloodBanks.FirstOrDefaultAsync(m => m.UserId == userId);
            var donation = new Donation
            {
                DonorId = donor.Id,
                Donor = donor,
                BloodBankId = bloodbank.Id,
                BloodBank = bloodbank
            };
            return View(donation);
        }

        // POST: Donations/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,DonorId,BloodBankId,BloodQuantity")] Donation donation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(new Donation 
                { 
                    DonorId = donation.DonorId,
                    BloodBankId = donation.BloodBankId,
                    BloodQuantity = donation.BloodQuantity
                });
                await _context.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                if (await _userManager.IsInRoleAsync(user, "BloodBank"))
                {
                    return RedirectToAction("BloodBankProfile", "Home");
                }
                else
                {
                    return RedirectToAction(nameof(Index));
                }
            }
            return View(donation);
        }

        // GET: Donations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donation = await _context.Donations.FindAsync(id);
            if (donation == null)
            {
                return NotFound();
            }
            return View(donation);
        }

        // POST: Donations/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,DonorId,BloodBankId,BloodQuantity")] Donation donation)
        {
            if (id != donation.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(donation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DonationExists(donation.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(donation);
        }

        // GET: Donations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var donation = await _context.Donations
                .FirstOrDefaultAsync(m => m.Id == id);
            if (donation == null)
            {
                return NotFound();
            }

            return View(donation);
        }

        // POST: Donations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var donation = await _context.Donations.FindAsync(id);
            _context.Donations.Remove(donation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DonationExists(int id)
        {
            return _context.Donations.Any(e => e.Id == id);
        }
    }
}
