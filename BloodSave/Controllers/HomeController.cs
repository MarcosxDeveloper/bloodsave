﻿using BloodSave.Data;
using BloodSave.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;

namespace BloodSave.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public HomeController(
            ILogger<HomeController> logger,
            ApplicationDbContext context,
            UserManager<IdentityUser> userManager)
        {
            _logger = logger;
            _context = context;
            _userManager = userManager;
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userManager.GetUserAsync(User);
                if(await _userManager.IsInRoleAsync(user, "Donor"))
                {
                    return RedirectToAction("DonorProfile");
                }
                else if (await _userManager.IsInRoleAsync(user, "BloodBank"))
                {
                    return RedirectToAction("BloodBankProfile");
                }
                else if (await _userManager.IsInRoleAsync(user, "Administrator"))
                {
                    return RedirectToAction("Index", "Donations");
                }

                return View();
            }

            return View();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Privacy()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = await _userManager.GetUserAsync(User);
                if (await _userManager.IsInRoleAsync(user, "Donor"))
                {
                    return RedirectToAction("DonorProfile");
                }
                else if (await _userManager.IsInRoleAsync(user, "BloodBank"))
                {
                    return RedirectToAction("BloodBankProfile");
                }
                else if(await _userManager.IsInRoleAsync(user, "Administrator"))
                {
                    return RedirectToAction("Index", "Donations");
                }

                return View();
            }

            return View();
        }

        [Authorize]
        public async Task<IActionResult> DonorProfile()
        {
            var userid = _userManager.GetUserId(User);
            var donor = await _context.Donors
                .Include(m => m.Donations)
                    .ThenInclude(m => m.BloodBank)
                .FirstOrDefaultAsync(m => m.UserId == userid);
            return View(donor);
        }

        [Authorize]
        public async Task<IActionResult> BloodBankProfile()
        {
            var userid = _userManager.GetUserId(User);
            var bloodbank = await _context.BloodBanks
                .Include(m => m.Donations)
                    .ThenInclude(m => m.Donor)
                .FirstOrDefaultAsync(m => m.UserId == userid);
            return View(bloodbank);
        }

        [Authorize]
        public async Task<IActionResult> GeoChart()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
